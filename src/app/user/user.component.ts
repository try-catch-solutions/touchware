import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
const USERS: User[] = [
  {
      id: 1,
      avatar: "user.png",
      rating: 3
  },
  {
      id: 2,
      avatar: "user.png",
      rating: 4
  },
  {
      id: 3,
      avatar: "user.png",
      rating: 2
  },
  {
      id: 4,
      avatar: "user.png",
      rating: 5
  },
];

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users = USERS;

  images:string[] = [];
  constructor() { }

  ngOnInit() {
  }

  createRange(number){
    var items: number[] = [];
    for(var i = 1; i <= number; i++){
       items.push(i);
    }
    return items;
  }

}
