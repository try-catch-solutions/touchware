import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private httpClient:HttpClient) { }

  ngOnInit() {
    
  }

  validateUser(user:any){
    let headers = new HttpHeaders({
      'Content-Type': 'application/json'
      });
    let options = {
      headers:headers
    }
    return this.httpClient.post('http://18.195.231.8:3000/api/v1/auth/appuser/login',user,options)
    .map((res:Response) => {
      return res;
    });

  }
}
