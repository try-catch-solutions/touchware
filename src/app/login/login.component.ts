import { Component, OnInit } from '@angular/core';
import {LoginService } from '../services/login.service';
import 'rxjs/add/operator/map';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[LoginService]
})
export class LoginComponent implements OnInit {

  email:string;
  password:string;
  isLoginError:boolean = false;
  

  constructor(private loginService:LoginService, private router: Router) { }

  
  ngOnInit() {
    
  }

  loginUser(event){
    event.preventDefault();
    let email = event.target.elements[0].value;
    let password = event.target.elements[1].value;
    let user = {
      email:email,
      password:password,
      notification_token:"123"
    }
    this.loginService.validateUser(user).subscribe((data:any) => {
      localStorage.setItem('userToken',data.accessToken);
      this.router.navigate(['/home']);
    },
    (err: HttpErrorResponse)=>{
      this.isLoginError = true;
    });

    };
            

}
