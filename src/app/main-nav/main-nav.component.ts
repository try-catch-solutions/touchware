import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit{

  ngOnInit(){
    
  }
  constructor(private router: Router) {}

  LogOut(){
    localStorage.removeItem('userToken');
    this.router.navigate(['/login']);
  }
  
  }
